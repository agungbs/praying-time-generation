        <!--  END SIDEBAR  -->
            <?php require "header.php" ?>

        <!--  BEGIN CONTENT AREA  -->
        
        <div class="layout-px-spacing">

                <div class="middle-content container-xxl p-0">
                    <div class="fq-header-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 align-self-center order-md-0 order-1">
                                    <div class="faq-header-content">
                                        <h1 class="mb-4">Prayer Time Generation</h1>
                                            <button class="btn btn-primary mb-4 btn-lg" id="subscriber" type="button" >Subscribe</button>
                                        <div class="col-lg-6 mx-auto">
                                            <div class="usr-tasks " >
                                                <div class="widget-content widget-content-area">
                                                    <h3 class="">Prayer Time</h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Subscribe Name</th>
                                                                    <th class="text-center">Created At</th>
                                                                    <th class="text-center">Box List</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <?php 
                                                               
                                                                    $result = $conn->query("SELECT * from subscriber");
                                                                      $no = 1;
                                                                     while($row = $result->fetch(PDO::FETCH_ASSOC))
                                                                     {

                                                                        $waktu_tertentu = new DateTime($row["created_at"]);
                                                                        $waktu_sekarang = new DateTime();
                                                                        $durasi = $waktu_tertentu->diff($waktu_sekarang);

                                                                        if ($durasi->format('%y') > 0) {
                                                                            $ago= $durasi->format('%y tahun yang lalu');
                                                                        } elseif ($durasi->format('%m') > 0) {
                                                                            $ago= $durasi->format('%m bulan yang lalu');
                                                                        } elseif ($durasi->format('%d') > 0) {
                                                                            $ago= $durasi->format('%d hari yang lalu');
                                                                        } elseif ($durasi->format('%h') > 0) {
                                                                            $ago= $durasi->format('%h jam yang lalu');
                                                                        } elseif ($durasi->format('%i') > 0) {
                                                                            $ago= $durasi->format('%i menit yang lalu');
                                                                        } else {
                                                                            $ago= 'baru saja';
                                                                        }
                                                                ?>
                                                                
                                                                <tr>
                                                                    <td><?php echo $row['subs_name']; ?></td>
                                                                    <td class="text-center"><?php echo $ago; ?></td>
                                                                    <td>
                                                                        <div class="action-btns">
                                                                            <a href="subscriber_box.php?subs_id=<?php echo $row['subs_id']; ?>" class="action-btn bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="View">
                                                                                <svg width="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M11.25 16.9999C11.25 16.5857 10.9142 16.2499 10.5 16.2499C10.0858 16.2499 9.75 16.5857 9.75 16.9999C9.75 17.4142 10.0858 17.7499 10.5 17.7499C10.9142 17.7499 11.25 17.4142 11.25 16.9999Z" fill="#1C274C"/>
                                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M8.67239 7.54199H15.3276C18.7024 7.54199 20.3898 7.54199 21.3377 8.52882C22.2855 9.51564 22.0625 11.0403 21.6165 14.0895L21.1935 16.9811C20.8437 19.3723 20.6689 20.5679 19.7717 21.2839C18.8745 21.9999 17.5512 21.9999 14.9046 21.9999H9.09534C6.4488 21.9999 5.12553 21.9999 4.22834 21.2839C3.33115 20.5679 3.15626 19.3723 2.80648 16.9811L2.38351 14.0895C1.93748 11.0403 1.71447 9.51565 2.66232 8.52882C3.61017 7.54199 5.29758 7.54199 8.67239 7.54199ZM12.75 10.4999C12.75 10.0857 12.4142 9.74995 12 9.74995C11.5858 9.74995 11.25 10.0857 11.25 10.4999V14.878C11.0154 14.7951 10.763 14.7499 10.5 14.7499C9.25736 14.7499 8.25 15.7573 8.25 16.9999C8.25 18.2426 9.25736 19.2499 10.5 19.2499C11.7426 19.2499 12.75 18.2426 12.75 16.9999V13.3197C13.4202 13.8633 14.2617 14.2499 15 14.2499C15.4142 14.2499 15.75 13.9142 15.75 13.4999C15.75 13.0857 15.4142 12.7499 15 12.7499C14.6946 12.7499 14.1145 12.5313 13.5835 12.0602C13.0654 11.6006 12.75 11.0386 12.75 10.4999Z" fill="#1C274C"/>
                                                                            <path opacity="0.4" d="M8.50956 2.00001H15.4897C15.7221 1.99995 15.9004 1.99991 16.0562 2.01515C17.164 2.12352 18.0708 2.78958 18.4553 3.68678H5.54395C5.92846 2.78958 6.83521 2.12352 7.94303 2.01515C8.09884 1.99991 8.27708 1.99995 8.50956 2.00001Z" fill="#1C274C"/>
                                                                            <path opacity="0.7" d="M6.3102 4.72266C4.91958 4.72266 3.77931 5.56241 3.39878 6.67645C3.39085 6.69967 3.38325 6.72302 3.37598 6.74647C3.77413 6.6259 4.18849 6.54713 4.60796 6.49336C5.68833 6.35485 7.05367 6.35492 8.6397 6.35501H15.5318C17.1178 6.35492 18.4832 6.35485 19.5635 6.49336C19.983 6.54713 20.3974 6.6259 20.7955 6.74647C20.7883 6.72302 20.7806 6.69967 20.7727 6.67645C20.3922 5.56241 19.2519 4.72266 17.8613 4.72266H6.3102Z" fill="#1C274C"/>
                                                                            </svg>
                                                                            </a>
                                                                        </div>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            <?php } ?> 

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MODAL -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Please complete the data</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                              <svg> ... </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="simple-example" method="POST" action="crud.php" novalidate>
                                <div class="form-row">
                                    <div class="col-md-12 mb-4">
                                        <label >Subscriber Name</label>
                                        <input type="text" class="form-control" name="subs_name" value="" >
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please fill the name
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn btn-light-dark" data-bs-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                                <button class="btn btn-primary submit-fn mt-2" name="BtnSaveSubscriber" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php require "footer.php" ?>


            <script type="text/javascript">
                $('#subscriber').click(function() {
                    $('#exampleModal').modal('show');
                });
            </script>
