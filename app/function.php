<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require '../vendor-phpmailer/autoload.php';

function sendEmail($BoxID, $PrayerTimeZone, $ErrorMessage) {

    $mail = new PHPMailer(true);

    try {
        // Konfigurasi SMTP
        $mail->isSMTP(); 
        $mail->Host = 'mail server';  
        $mail->SMTPAuth = true;                              
        $mail->Username = 'email sender';                
        $mail->Password = 'email password';                          
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->Port = 465; 

        $mail->addAddress('phu@expressinmusic.com');

        $mail->isHTML(true); 
        $mail->Subject = 'Error Message';
        $mail->Body = "BoxID: $BoxID<br>PrayerTimeZone: $PrayerTimeZone<br>ErrorMessage: $ErrorMessage";

        $mail->send();
        return "Email sent successfully!";
    } catch (Exception $e) {
        return "Error sending email: {$mail->ErrorInfo}";
    }
}


function postDataToAPI($datestart, $dateend, $prayerzone) {
    $url = 'https://www.e-solat.gov.my/index.php?r=esolatApi/takwimsolat&period=duration&zone='.$prayerzone; // URL API target
    $postData = array(
        'datestart' => $datestart,
        'dateend' => $dateend
    );

    $ch = curl_init($url); // Inisialisasi cURL

    // Set opsi cURL
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));

    // Eksekusi cURL untuk mendapatkan respons
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Mendapatkan HTTP code

    // Tutup koneksi cURL
    curl_close($ch);

    return array(
        'httpCode' => $httpCode,
        'response' => $response
    );
}