        <!--  END SIDEBAR  -->
            <?php 

                $box_id=$_GET['box_id'];
                require "header.php";
                require 'function.php';

                $result=$conn->query("SELECT  song_title,prayertime from song 
                                    where box_id='$box_id' and prayertimedate=CURRENT_DATE() and prayertime>CURRENT_TIME() order by prayertime asc
                                    LIMIT 1");
                $row = $result->fetch(PDO::FETCH_ASSOC);
                $song_title_for_alert=$row['song_title']??"";
                $prayertime_for_alert=$row['prayertime']??"";

                $result=$conn->query("SELECT box_name,prayerzone,status,subs_id,status FROM box where box_id='$box_id' ");
                $row = $result->fetch(PDO::FETCH_ASSOC);
                $box_name=$row['box_name'];
                $prayerzone=$row['prayerzone'];
                $subs_id=$row['subs_id'];
                $status_box=$row['status'];

                if ($row['status']==0) {
                   $checker="";
                }else{
                   $checker="Checked";
                }
                
                $success=1;
                $error="";
                $datestart = date('Y-m-d');
                $dateend = date('Y-m-d', strtotime($datestart . ' + ' . 6 . ' days'));
                $apiResponse = postDataToAPI($datestart, $dateend, $prayerzone);
                if ($apiResponse['httpCode'] !== 200) {
                    $success=0;
                    $error="HTTP Code: {$apiResponse['httpCode']} - Respons dari API tidak berhasil.";
                    $send_error = sendEmail($box_id, $prayerzone, "HTTP Code: {$apiResponse['response']} - The response from the API was unsuccessful.");
                } else {
                    $data = json_decode($apiResponse['response'], true);
                }

                $no=1;

                try {

                    //sync the song data base on current date
                    $stmt = $conn->prepare("DELETE FROM song WHERE box_id = :box_id AND subs_id = :subs_id");
                    $stmt->bindParam(':box_id', $box_id);
                    $stmt->bindParam(':subs_id', $subs_id);
                    $stmt->execute();

                    // Loop for each data into 'prayerTime'
                    foreach ($data['prayerTime'] as $prayTime) {
                        $elements = ['imsak', 'fajr', 'syuruk', 'dhuhr', 'asr', 'maghrib', 'isha'];

                        foreach ($elements as $element) {
                            
                            $stmt = $conn->prepare("INSERT INTO song (song_title, prayertime, subs_id, box_id, prayerzone, prayertimedate, prayertimeseq, prayertimedatehijri) VALUES (:song_title, :prayertime, :subs_id, :box_id, :prayerzone, :prayertimedate, :prayertimeseq, :prayertimedatehijri)");
                            
                            // Bind parameters
                            $stmt->bindParam(':song_title', $song_title);
                            $stmt->bindParam(':prayertime', $prayertime);
                            $stmt->bindParam(':subs_id', $subs_id);
                            $stmt->bindParam(':box_id', $box_id);
                            $stmt->bindParam(':prayerzone', $prayerzone);
                            $stmt->bindParam(':prayertimedate', $prayertimedate);
                            $stmt->bindParam(':prayertimeseq', $prayertimeseq);
                            $stmt->bindParam(':prayertimedatehijri', $prayertimedatehijri);
                            
                            // Set parameter value
                            $song_title = ucwords($element). " (".date('m-d',strtotime($prayTime['date'])).")";
                            $prayertime = $prayTime[$element];
                            $subs_id = $subs_id; 
                            $box_id = $box_id; 
                            $prayerzone = $prayerzone; 
                            $prayertimedate = date("Y-m-d",strtotime($prayTime['date'])); 
                            $prayertimeseq = $no++; 
                            $prayertimedatehijri = $prayTime['hijri']; 
                            $stmt->execute();
                        }
                    }

                } catch(PDOException $e) {
                    // Error response
                    $success=0;
                    $response = "Error updating status: " . $e->getMessage();
                    $error= "$response";

                    try {
                        $send_error = sendEmail($box_id, $prayerzone, $response);
                    } catch (Exception $e) {
                        $error= "Error: $response<br>" . $e->getMessage();
                    }

                }

                

            ?>

        <style type="text/css">
            @keyframes blink {
              0% { background-color: transparent; }
              50% { background-color: yellow; }
              100% { background-color: transparent; }
            }

            .blinking-row {
              animation: blink 2s infinite;
            }
        </style>
        
        <div class="layout-px-spacing">

                <div class="middle-content container-xxl p-0">
                    <div class="fq-header-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 align-self-center order-md-0 order-1">
                                    <div class="faq-header-content">
                                        <h1 class="mb-4"><?php echo $box_name; ?></h1>
                                        <h2 class="mb-4"><?php echo $prayerzone; ?></h2>
                                        <h2 class="mb-4" id="clock"></h2>
                                        <div class="switch form-switch-custom switch-inline form-switch-primary form-switch-custom dual-label-toggle">
                                            <label class="switch-label switch-label-left" for="form-custom-switch-dual-label">Disable</label>
                                            <div class="input-checkbox">
                                                <input class="switch-input form-custom-switch-dual-label" type="checkbox" role="switch" <?php echo $checker; ?>>
                                            </div>
                                            <label class="switch-label switch-label-right" for="form-custom-switch-dual-label">Enable</label>
                                        </div>
                                        <br>
                                        <br>

                                        <div class="col-lg-6 mx-auto">
                                            <div class="usr-tasks " >
                                                <div class="widget-content widget-content-area">
                                                    <h3 class="">Praying Time</h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Event Pray</th>
                                                                    <th>Time</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <?php 
                                                                    $time=date("H:i:s");
                                                                    $result = $conn->query("SELECT song_title,prayertime from song 
                                                                        where box_id='$box_id' and prayertimedate=CURRENT_DATE()");
                                                                     while($row = $result->fetch(PDO::FETCH_ASSOC))
                                                                     {
                                                                        if ($time>$row['prayertime']) {
                                                                            $class='class="bg-success"';
                                                                        }else{
                                                                            $class='';
                                                                        }

                                                                        if ($row['prayertime']==$prayertime_for_alert) {
                                                                            $class='class="blinking-row"';
                                                                        }

                                                                ?>
                                                                
                                                                <tr <?php echo $class; ?>>
                                                                    <td><?php echo $row['song_title']; ?></td>
                                                                    <td><?php echo $row['prayertime']; ?></td>
                                                                </tr>
                                                            <?php } ?> 

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6 mx-auto">
                                            <div class="usr-tasks " >
                                                <div class="widget-content widget-content-area">
                                                    <h3 class="">Praying Schedule</h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Day</th>
                                                                    <th>Date</th>
                                                                    <th>Hijri</th>
                                                                    <th>Praying Time</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <?php 
                                                               
                                                                    $result = $conn->query("SELECT DAYNAME(prayertimedate) AS day,prayertimedate,prayertimedatehijri,box_id from song 
                                                                        where box_id='$box_id' group by day order by prayertimedate asc");
                                                                     while($row = $result->fetch(PDO::FETCH_ASSOC))
                                                                     {

                                                                ?>
                                                                
                                                                <tr>
                                                                    <td><?php echo $row['day']; ?></td>
                                                                    <td><?php echo $row['prayertimedate']; ?></td>
                                                                    <td><?php echo $row['prayertimedatehijri']; ?></td>
                                                                     <td><div class="action-btns">
                                                                            <a data-box_id="<?php echo $row['box_id']; ?>" data-at_date="<?php echo $row['prayertimedate']; ?>" href="javascript:void(0);" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="View">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?> 

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MODAL -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Time To Pray</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                              <svg> ... </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h3 id="event_pray"></h3>
                            <audio id="myAudio">
                              <source src="Time to Pray.mp3" type="audio/mpeg" allow="autoplay">
                              Your browser does not support the audio element.
                            </audio>
                            <svg width="400px" height="400px" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
                            <g id="pray-day">

                            <circle cx="39.5" cy="27.5" r="21.5" style="fill:#e5efef"/>

                            <polygon points="58 61 6 61 12 43 52 43 58 61" style="fill:#fc8c29;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <circle cx="13" cy="27" r="1" style="fill:#4c241d"/>

                            <path d="M38.243,43.029H33.471l.4-4.587a9.378,9.378,0,0,0-.187-2.855L32,28l-2-9-5-3L21.273,40.311,21,43.994a5.594,5.594,0,0,0,5.23,5.582L33,50,15,53l-3.029,3H38.515A6.485,6.485,0,0,0,45,49.315,6.666,6.666,0,0,0,38.243,43.029Z" style="fill:#6b4f5b;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="52" y1="7" x2="52" y2="10" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="52" y1="14" x2="52" y2="17" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="50" y1="12" x2="47" y2="12" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="57" y1="12" x2="54" y2="12" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <circle cx="43" cy="4" r="1" style="fill:#4c241d"/>

                            <path d="M28.59,18.911l4.823-1.234-1.838-8.45a5.355,5.355,0,0,0-6.3-3.857h0a5.042,5.042,0,0,0-3.54,6.186L22.893,16.2a4.093,4.093,0,0,0,1.7,2.435" style="fill:#e7d1c4;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <path d="M21.9,4.094h7.23A2.071,2.071,0,0,1,31.2,6.166V9.094a0,0,0,0,1,0,0H19.833a0,0,0,0,1,0,0V6.166A2.071,2.071,0,0,1,21.9,4.094Z" transform="translate(-0.792 5.037) rotate(-11.102)" style="fill:#6b4f5b;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <path d="M43.559,29.463l-6.5,3.035-5.707-7.836c-1.039-1.426-2.786-1.923-3.9-1.11h0c-1.116.813-1.179,2.628-.14,4.054l6.09,8.361c1.451,1.992,3.891,2.686,5.45,1.551l6.421-4.677Z" style="fill:#6b4f5b;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <path d="M45.268,32.841l4.165-3.034a1.412,1.412,0,0,0-1.428-2.42l-4.446,2.076Z" style="fill:#e7d1c4;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="38" y1="50" x2="36" y2="50" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="60" y1="52" x2="55" y2="52" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="63" y1="61" x2="58" y2="61" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="56" y1="43" x2="52" y2="43" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="4" y1="52" x2="9" y2="52" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="1" y1="61" x2="6" y2="61" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            <line x1="8" y1="43" x2="12" y2="43" style="fill:none;stroke:#4c241d;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px"/>

                            </g>

                            </svg>

                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Praying Time at <span id="at_date"></span></h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                              <svg> ... </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="result_praying_time"></div>

                        </div>
                    </div>
                </div>
            </div>

            <?php require "footer.php" ?>


            <script type="text/javascript">

                $('.btn-view').click(function() {
                    var box_id = $(this).data('box_id');
                    var at_date = $(this).data('at_date');
                    $.ajax({
                        type: 'POST',
                        url: 'ajax/praying_time.php',
                        data: {
                            'box_id':  box_id,'at_date':  at_date
                        },
                        success: function (data) {
                            $("#result_praying_time").html(data);
                            $("#at_date").html(at_date);
                            $('#exampleModal2').modal('show');
                        }
                    });
                });
                    
                document.addEventListener('DOMContentLoaded', function() {
                    var modal = document.getElementById('exampleModal');

                    modal.addEventListener('shown.bs.modal', function() {
                      var audio = document.getElementById('myAudio');
                      audio.play();
                    });
                  });

                document.addEventListener('DOMContentLoaded', function() {

                    <?php if ($success==0) {
                        echo "Swal.fire({
                            icon: 'error',
                            title: '".$error."',
                        })";
                    } ?>
                  
                  function updateClock() {
                    // Create a new Date object to get the current date and time
                    const now = new Date();

                    // Get the current time components
                    const hours = now.getHours().toString().padStart(2, '0'); // Ensure 2-digit format
                    const minutes = now.getMinutes().toString().padStart(2, '0');
                    const seconds = now.getSeconds().toString().padStart(2, '0');

                    // Combine the components into the desired format
                    const currentTime = hours + ':' + minutes + ':' + seconds;

                    // Display the current time
                    const clockElement = document.getElementById('clock');
                    clockElement.textContent = currentTime; // Update the clock element

                    if (currentTime=="<?php echo $prayertime_for_alert; ?>") {
                        $("#event_pray").html("<?php echo $song_title_for_alert ?> - <?php echo $prayertime_for_alert ?>");
                        <?php if ($status_box==1) {
                            echo "$('#exampleModal').modal('show');";
                        } ?>

                        setTimeout(function() {
                            location.reload();
                        }, 10000);
                      
                    }
                  }
                  // Call updateClock every second (1000 milliseconds)
                  setInterval(updateClock, 1000);
                });
                $('.form-custom-switch-dual-label').change(function(){
                    var box_id = <?php echo $box_id; ?>;
                    if($(this).prop('checked')){
                        var status=1;
                    } else {
                        var status=0;
                    }
                    $.ajax({
                        type: 'POST',
                        url: 'ajax/update_status_box.php',
                        data: {
                            'box_id':  box_id,'status':  status
                        },
                        dataType : 'json',
                        success: function (data) {
                            if (data.success) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000,
                                    timerProgressBar: true,
                                    didOpen: (toast) => {
                                        toast.addEventListener('mouseenter', Swal.stopTimer)
                                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })
                                    
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Status changed'
                                })
                            } else {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000,
                                    timerProgressBar: true,
                                    didOpen: (toast) => {
                                        toast.addEventListener('mouseenter', Swal.stopTimer)
                                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })
                                    
                                Toast.fire({
                                    icon: 'error',
                                    title: '"Error: " + data.message'
                                })
                            }
                        },
                        error: function(xhr, status, error) {
                            // Handle AJAX call errors
                            console.error("AJAX Error: " + status + " - " + error);
                            alert("An error occurred while processing the request.");
                        }
                    });
                });
            </script>
