<?php 
require "dbconn.php"; 
require_once('../vendor/autoload.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
require '../vendor-phpmailer/autoload.php';


if(isset($_POST['BtnSaveBox']))
{
	// Validate and sanitize user inputs
	$box_name = isset($_POST['box_name']) ? $_POST['box_name'] : '';
	$box_name = filter_var($box_name, FILTER_SANITIZE_STRING);

	$prayerzone = isset($_POST['prayerzone']) ? $_POST['prayerzone'] : '';
	$prayerzone = filter_var($prayerzone, FILTER_SANITIZE_STRING);

	$subs_id = isset($_POST['subs_id']) ? $_POST['subs_id'] : '';
	$subs_id = filter_var($subs_id, FILTER_VALIDATE_INT);

	if ($box_name && $prayerzone && $subs_id !== false && $subs_id !== null) {
	    // Prepare the SQL statement with parameterized query
	    $sql = "INSERT INTO box (box_name, prayerzone, subs_id, created_at) VALUES (:box_name, :prayerzone, :subs_id, NOW())";
	    $stmt = $conn->prepare($sql);

	    // Bind parameters
	    $stmt->bindParam(':box_name', $box_name);
	    $stmt->bindParam(':prayerzone', $prayerzone);
	    $stmt->bindParam(':subs_id', $subs_id, PDO::PARAM_INT);

	    // Execute the statement
	    if ($stmt->execute()) {
	        // Redirect after successful insert
	        header("Location: subscriber_box.php?subs_id=$subs_id");
	        exit(); // Always exit after a header redirect
	    } else {
	        echo "Error inserting record";
	    }
	} else {
	    echo "Invalid input"; // Handle invalid input gracefully
	}


}


if(isset($_POST['BtnSaveSubscriber']))
{
	
	// Validate and sanitize user input
		$subs_name = isset($_POST['subs_name']) ? $_POST['subs_name'] : '';
		$subs_name = filter_var($subs_name, FILTER_SANITIZE_STRING);

		if (!empty($subs_name)) {
		    // Prepare the SQL statement with parameterized query
		    $sql = "INSERT INTO subscriber (subs_name, created_at) VALUES (:subs_name, NOW())";
		    $stmt = $conn->prepare($sql);

		    // Bind parameters
		    $stmt->bindParam(':subs_name', $subs_name);

		    // Execute the statement
		    if ($stmt->execute()) {
		        $last_id = $conn->lastInsertId();
		        // Redirect after successful insert
		        header("Location: subscriber_box.php?subs_id=$last_id");
		        exit(); // Always exit after a header redirect
		    } else {
		        echo "Error inserting record";
		    }
		} else {
		    echo "Invalid input"; // Handle invalid input gracefully
		}


}


?>