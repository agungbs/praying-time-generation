<?php
// Check if the request method is POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if box_id and status are set in the POST data
    if (isset($_POST['box_id']) && isset($_POST['status'])) {
        // Include your database connection file
        include_once "../dbconn.php";
        $box_id = $_POST['box_id'];
        $status = $_POST['status'];

        try {

            // Prepare update query
            $stmt = $conn->prepare("UPDATE box SET status=:status WHERE box_id=:box_id");
            $stmt->bindParam(':status', $status);
            $stmt->bindParam(':box_id', $box_id);
            
            // Execute the update query
            $stmt->execute();

            // Success response
            $response['success'] = true;
            $response['message'] = "Status updated successfully";
            echo json_encode($response);
        } catch(PDOException $e) {
            // Error response
            $response['success'] = false;
            $response['message'] = "Error updating status: " . $e->getMessage();
            echo json_encode($response);
        }

        // Close database connection
        $conn = null;
    } else {
        // Required parameters not provided
        $response['success'] = false;
        $response['message'] = "Missing parameters: box_id or status";
        echo json_encode($response);
    }
} else {
    // Invalid request method
    $response['success'] = false;
    $response['message'] = "Invalid request method";
    echo json_encode($response);
}

?>
