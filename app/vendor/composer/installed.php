<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nicolab/php-ftp-client' => array(
            'pretty_version' => 'v2.0.2',
            'version' => '2.0.2.0',
            'reference' => 'a1d007c8b203895611f68b0da314281d4a5c3d49',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nicolab/php-ftp-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
