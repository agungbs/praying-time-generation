        <!--  END SIDEBAR  -->
            <?php 

                $subs_id=$_GET['subs_id'];
                require "header.php";

                $result=$conn->query("SELECT subs_name FROM subscriber  where subs_id='$subs_id' ");
                $row = $result->fetch(PDO::FETCH_ASSOC);
                $subs_name=$row['subs_name'];

            ?>

        <!--  BEGIN CONTENT AREA  -->
        
        <div class="layout-px-spacing">

                <div class="middle-content container-xxl p-0">
                    <div class="fq-header-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 align-self-center order-md-0 order-1">
                                    <div class="faq-header-content">
                                        <h1 class="mb-4">Prayer Time Generation</h1>
                                        <h2 class="mb-4"><?php echo $subs_name; ?></h2>
                                            <button class="btn btn-primary mb-4 btn-lg" id="subscriber" type="button" >Add a New Box</button>
                                        <div class="col-lg-6 mx-auto">
                                            <div class="usr-tasks " >
                                                <div class="widget-content widget-content-area">
                                                    <h3 class="">Box List</h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Box Name</th>
                                                                    <th>Prayer Zone</th>
                                                                    <th >Created At</th>
                                                                    <th>Status</th>
                                                                    <th>Praying Time</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                 <?php 
                                                               
                                                                    $result = $conn->query("SELECT box_id,box_name,prayerzone,created_at,status from box 
                                                                        where subs_id='$subs_id'");
                                                                      $no = 1;
                                                                     while($row = $result->fetch(PDO::FETCH_ASSOC))
                                                                     {

                                                                        $waktu_tertentu = new DateTime($row["created_at"]);
                                                                        $waktu_sekarang = new DateTime();
                                                                        $durasi = $waktu_tertentu->diff($waktu_sekarang);

                                                                        if ($durasi->format('%y') > 0) {
                                                                            $ago= $durasi->format('%y tahun yang lalu');
                                                                        } elseif ($durasi->format('%m') > 0) {
                                                                            $ago= $durasi->format('%m bulan yang lalu');
                                                                        } elseif ($durasi->format('%d') > 0) {
                                                                            $ago= $durasi->format('%d hari yang lalu');
                                                                        } elseif ($durasi->format('%h') > 0) {
                                                                            $ago= $durasi->format('%h jam yang lalu');
                                                                        } elseif ($durasi->format('%i') > 0) {
                                                                            $ago= $durasi->format('%i menit yang lalu');
                                                                        } else {
                                                                            $ago= 'baru saja';
                                                                        }

                                                                        if ($row['status']==0) {
                                                                           $checker="";
                                                                        }else{
                                                                           $checker="Checked";
                                                                        }
                                                                ?>
                                                                
                                                                <tr>
                                                                    <td><?php echo $row['box_name']; ?></td>
                                                                    <td><?php echo $row['prayerzone']; ?></td>
                                                                    <td><?php echo $ago; ?></td>
                                                                    <td><div class="switch form-switch-custom switch-inline form-switch-primary form-switch-custom dual-label-toggle">
                                                                            <label class="switch-label switch-label-left" for="form-custom-switch-dual-label">Disable</label>
                                                                            <div class="input-checkbox">
                                                                                <input class="switch-input form-custom-switch-dual-label" value="<?php echo $row['box_id']; ?>" type="checkbox" role="switch" <?php echo $checker; ?>>
                                                                            </div>
                                                                            <label class="switch-label switch-label-right" for="form-custom-switch-dual-label">Enable</label>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="action-btns">
                                                                            <a  href="box_song.php?box_id=<?php echo $row['box_id']; ?>" class="action-btn bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="View">
                                                                                <svg fill="#000000" height="800px" width="800px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
                                                                                 viewBox="0 0 487.617 487.617" xml:space="preserve">
                                                                            <g>
                                                                                <path d="M180.069,178.503c2.136-0.961,3.092-3.47,2.136-5.609c-0.957-2.142-3.471-3.103-5.614-2.146
                                                                                    c-1.823,0.815-44.765,20.772-54.005,91.919c-0.302,2.328,1.34,4.459,3.668,4.762c0.186,0.024,0.37,0.036,0.553,0.036
                                                                                    c2.104,0,3.931-1.561,4.21-3.703c4.496-34.621,17.6-55.956,27.8-67.758C169.798,183.297,179.876,178.592,180.069,178.503z"/>
                                                                                <path d="M334.908,356.742c-3.684-2.011-7.763-3.933-12.081-5.966c-14.645-6.898-31.124-14.669-42.76-30.335l80.079-67.695
                                                                                    c4.798-4.04,7.737-9.707,8.273-15.956c0.405-4.723-0.599-9.349-2.856-13.399c3.282-3.752,5.466-8.514,6.094-13.54
                                                                                    c0.819-6.564-0.955-12.822-4.994-17.619l-8.258-9.809c-4.04-4.798-9.706-7.736-15.956-8.273
                                                                                    c-6.245-0.537-12.333,1.393-17.162,5.458l-69.233,59.401c-0.222-6.752-0.691-14.658-1.622-22.791
                                                                                    c-2.526-22.083-7.525-37.544-14.906-46.21c8.66-1.594,17.042-4.778,24.778-9.512c16.542-10.123,28.152-26.083,32.691-44.938
                                                                                    c5.838-24.251-1.383-50.12-18.844-67.908l-4.29-35.874C273.059,5.063,267.359,0,260.604,0c-0.531,0-1.069,0.032-1.599,0.096
                                                                                    l-90.498,10.823c-7.316,0.877-12.556,7.541-11.681,14.856l4.785,40.019c-2.523,5-4.476,10.299-5.795,15.781
                                                                                    c-6.061,25.175,1.871,50.492,18.751,67.779c-5.607,3.372-15.496,10.271-26.029,22.085c-14.439,16.195-32.908,45.167-38.889,91.227
                                                                                    c-4.356,33.533-6.55,82.092-1.075,109.468l0.277,1.39c3.278,16.479,7.452,37.429,32.424,46.139
                                                                                    c-9.828,2.833-19.816,7.37-27.312,14.631c-8.123,7.868-12.241,17.847-12.241,29.659v19.414c0,1.129,0.449,2.211,1.248,3.008
                                                                                    c0.797,0.796,1.877,1.242,3.002,1.242c0.003,0,0.005,0,0.008,0l207.658-0.377c20.713,0,38.17-4.446,50.485-12.856
                                                                                    c14.243-9.728,21.772-24.872,21.772-43.794C385.895,390.615,374.41,378.307,334.908,356.742z M359.951,236.063
                                                                                    c-0.342,3.987-2.217,7.603-5.285,10.186l-90.994,76.923c-2.697,2.272-6.12,3.523-9.639,3.522c-4.437,0-8.617-1.944-11.471-5.333
                                                                                    l-74.356-88.317c-2.577-3.061-3.808-6.943-3.466-10.93c0.256-2.982,1.37-5.755,3.196-8.043l39.989,49.888
                                                                                    c0.839,1.047,2.073,1.592,3.318,1.592c0.933,0,1.872-0.305,2.656-0.934c1.831-1.468,2.126-4.143,0.658-5.974l-40.301-50.277
                                                                                    l5.57-4.69c2.697-2.271,6.121-3.521,9.64-3.521c4.437,0,8.617,1.944,11.471,5.333l53.556,63.613
                                                                                    c0.726,0.862,1.765,1.401,2.888,1.497c1.125,0.096,2.239-0.258,3.101-0.983l66.634-56.101c2.697-2.271,6.121-3.522,9.64-3.522
                                                                                    c4.436,0,8.617,1.944,11.47,5.333l8.259,9.81C359.062,228.194,360.293,232.076,359.951,236.063z M330.793,186.086
                                                                                    c3.061-2.577,6.944-3.807,10.93-3.466c3.987,0.342,7.602,2.217,10.18,5.278l8.258,9.809c2.497,2.965,3.584,6.904,3.062,11.092
                                                                                    c-0.344,2.76-1.392,5.392-2.96,7.624l-5.535-6.574c-4.472-5.312-11.023-8.358-17.972-8.358c-5.519,0-10.887,1.96-15.114,5.52
                                                                                    l-63.383,53.364l-2.14-2.542c0.044-1.393,0.109-4.107,0.112-7.773L330.793,186.086z M169.517,19.358l90.497-10.823
                                                                                    c0.198-0.023,0.395-0.035,0.589-0.035c2.455,0,4.526,1.843,4.819,4.288l3.976,33.255l-99.718,15.646l-1.411-11.797l32.865-4.449
                                                                                    c2.326-0.314,3.957-2.456,3.642-4.781c-0.314-2.326-2.456-3.959-4.781-3.642l-32.735,4.431l-1.995-16.685
                                                                                    C164.947,22.103,166.855,19.678,169.517,19.358z M164.079,83.564c1.095-4.548,2.678-8.956,4.712-13.132l103.826-16.29
                                                                                    c15.042,15.689,21.211,38.26,16.115,59.429c-4.008,16.648-14.258,30.739-28.864,39.677c-14.606,8.938-31.818,11.653-48.466,7.645
                                                                                    C177.035,152.62,155.806,117.93,164.079,83.564z M167.895,415.14c-41,2.112-46.5-22.123-50.707-43.275l-0.279-1.398
                                                                                    c-5.286-26.432-3.1-73.842,1.169-106.706c5.67-43.66,22.972-71.033,36.488-86.308c11.661-13.179,22.588-19.88,26.629-22.098
                                                                                    c8.043,6.423,17.568,11.239,28.218,13.803c5.684,1.369,11.426,2.045,17.127,2.045c0.302,0,0.604-0.012,0.906-0.016
                                                                                    c16.552,6.876,20.211,50.9,20.319,76.723l-40.325-47.898c-4.473-5.312-11.023-8.358-17.973-8.358c-5.519,0-10.887,1.96-15.114,5.52
                                                                                    l-9.808,8.258c-4.798,4.04-7.736,9.706-8.273,15.955c-0.536,6.249,1.393,12.333,5.433,17.131l74.355,88.317
                                                                                    c4.472,5.313,11.022,8.359,17.973,8.359c0.001,0,0.001,0,0.001,0c5.519,0,10.887-1.96,15.12-5.525l4.411-3.729
                                                                                    c12.914,17.105,30.953,25.606,45.642,32.526c4.2,1.978,8.167,3.847,11.629,5.738c7.553,4.123,13.911,7.82,19.267,11.347h-13.361
                                                                                    c-2.347,0-4.25,1.903-4.25,4.25c0,2.347,1.903,4.25,4.25,4.25h24.631c3.26,2.918,5.885,5.868,7.997,9h-38.128
                                                                                    c-2.347,0-4.25,1.903-4.25,4.25c0,2.347,1.903,4.25,4.25,4.25h42.468c2.82,7.67,3.685,16.819,3.685,29.04
                                                                                    c0,41.895-39.945,48.15-63.764,48.15l-203.408,0.369V464.05h42.519c2.347,0,4.25-1.903,4.25-4.25c0-2.347-1.903-4.25-4.25-4.25
                                                                                    h-41.593c1.365-5.809,4.251-10.807,8.716-15.137c13.181-12.783,36.589-16.067,48.982-16.779
                                                                                    c64.425-3.698,85.376-1.602,91.316-0.632c1.738,0.283,3.239,0.528,5.246,0.528c2.347,0,4.25-1.903,4.25-4.25
                                                                                    c0-2.347-1.903-4.25-4.25-4.25c-1.318,0-2.281-0.157-3.877-0.417C254.823,413.518,233.095,411.595,167.895,415.14z"/>
                                                                                <path d="M224.241,455.55h-49c-2.347,0-4.25,1.903-4.25,4.25c0,2.347,1.903,4.25,4.25,4.25h49c2.347,0,4.25-1.903,4.25-4.25
                                                                                    C228.491,457.453,226.588,455.55,224.241,455.55z"/>
                                                                            </g>
                                                                            </svg>
                                                                            </a>
                                                                        </div>
                                                                        
                                                                    </td>

                                                                </tr>
                                                            <?php } ?> 

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MODAL -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Please complete the data</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                              <svg> ... </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="simple-example" method="POST" action="crud.php" novalidate>
                                <div class="form-row">
                                    <div class="col-md-12 mb-4">
                                        <label >Box Name</label>
                                        <input type="hidden" class="form-control" name="subs_id" value="<?php echo $subs_id; ?>" >
                                        <input type="text" class="form-control" name="box_name" value="" >
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please fill the name
                                        </div>
                                    </div>

                                    <div class="col-md-12 mb-4">
                                        <label >Prayer Zone</label>
                                        <select name="prayerzone" class="form-select">
                          <option selected>-Choose-</option>
                          <optgroup label="Johor"><option value='JHR01' class='hs'>JHR01 - Pulau Aur dan Pulau Pemanggil </option><option value='JHR02' class='hs'>JHR02 - Johor Bahru, Kota Tinggi, Mersing, Kulai</option><option value='JHR03' class='hs'>JHR03 - Kluang, Pontian</option><option value='JHR04' class='hs'>JHR04 - Batu Pahat, Muar, Segamat, Gemas Johor, Tangkak</option></optgroup><optgroup label="Kedah"><option value='KDH01' class='hs'>KDH01 - Kota Setar, Kubang Pasu, Pokok Sena (Daerah Kecil)</option><option value='KDH02' class='hs'>KDH02 - Kuala Muda, Yan, Pendang</option><option value='KDH03' class='hs'>KDH03 - Padang Terap, Sik</option><option value='KDH04' class='hs'>KDH04 - Baling</option><option value='KDH05' class='hs'>KDH05 - Bandar Baharu, Kulim</option><option value='KDH06' class='hs'>KDH06 - Langkawi</option><option value='KDH07' class='hs'>KDH07 - Puncak Gunung Jerai</option></optgroup><optgroup label="Kelantan"><option value='KTN01' class='hs'>KTN01 - Bachok, Kota Bharu, Machang, Pasir Mas, Pasir Puteh, Tanah Merah, Tumpat, Kuala Krai, Mukim Chiku</option><option value='KTN02' class='hs'>KTN02 - Gua Musang (Daerah Galas Dan Bertam), Jeli, Jajahan Kecil Lojing</option></optgroup><optgroup label="Melaka"><option value='MLK01' class='hs'>MLK01 - SELURUH NEGERI MELAKA</option></optgroup><optgroup label="Negeri Sembilan"><option value='NGS01' class='hs'>NGS01 - Tampin, Jempol</option><option value='NGS02' class='hs'>NGS02 - Jelebu, Kuala Pilah, Rembau</option><option value='NGS03' class='hs'>NGS03 - Port Dickson, Seremban</option></optgroup><optgroup label="Pahang"><option value='PHG01' class='hs'>PHG01 - Pulau Tioman</option><option value='PHG02' class='hs'>PHG02 - Kuantan, Pekan, Rompin, Muadzam Shah</option><option value='PHG03' class='hs'>PHG03 - Jerantut, Temerloh, Maran, Bera, Chenor, Jengka</option><option value='PHG04' class='hs'>PHG04 - Bentong, Lipis, Raub</option><option value='PHG05' class='hs'>PHG05 - Genting Sempah, Janda Baik, Bukit Tinggi</option><option value='PHG06' class='hs'>PHG06 - Cameron Highlands, Genting Higlands, Bukit Fraser</option></optgroup><optgroup label="Perlis"><option value='PLS01' class='hs'>PLS01 - Kangar, Padang Besar, Arau</option></optgroup><optgroup label="Pulau Pinang"><option value='PNG01' class='hs'>PNG01 - Seluruh Negeri Pulau Pinang</option></optgroup><optgroup label="Perak"><option value='PRK01' class='hs'>PRK01 - Tapah, Slim River, Tanjung Malim</option><option value='PRK02' class='hs'>PRK02 - Kuala Kangsar, Sg. Siput , Ipoh, Batu Gajah, Kampar</option><option value='PRK03' class='hs'>PRK03 - Lenggong, Pengkalan Hulu, Grik</option><option value='PRK04' class='hs'>PRK04 - Temengor, Belum</option><option value='PRK05' class='hs'>PRK05 - Kg Gajah, Teluk Intan, Bagan Datuk, Seri Iskandar, Beruas, Parit, Lumut, Sitiawan, Pulau Pangkor</option><option value='PRK06' class='hs'>PRK06 - Selama, Taiping, Bagan Serai, Parit Buntar</option><option value='PRK07' class='hs'>PRK07 - Bukit Larut</option></optgroup><optgroup label="Sabah"><option value='SBH01' class='hs'>SBH01 - Bahagian Sandakan (Timur), Bukit Garam, Semawang, Temanggong, Tambisan, Bandar Sandakan, Sukau</option><option value='SBH02' class='hs'>SBH02 - Beluran, Telupid, Pinangah, Terusan, Kuamut, Bahagian Sandakan (Barat)</option><option value='SBH03' class='hs'>SBH03 - Lahad Datu, Silabukan, Kunak, Sahabat, Semporna, Tungku, Bahagian Tawau  (Timur)</option><option value='SBH04' class='hs'>SBH04 - Bandar Tawau, Balong, Merotai, Kalabakan, Bahagian Tawau (Barat)</option><option value='SBH05' class='hs'>SBH05 - Kudat, Kota Marudu, Pitas, Pulau Banggi, Bahagian Kudat</option><option value='SBH06' class='hs'>SBH06 - Gunung Kinabalu</option><option value='SBH07' class='hs'>SBH07 - Kota Kinabalu, Ranau, Kota Belud, Tuaran, Penampang, Papar, Putatan, Bahagian Pantai Barat</option><option value='SBH08' class='hs'>SBH08 - Pensiangan, Keningau, Tambunan, Nabawan, Bahagian Pendalaman (Atas)</option><option value='SBH09' class='hs'>SBH09 - Beaufort, Kuala Penyu, Sipitang, Tenom, Long Pasia, Membakut, Weston, Bahagian Pendalaman (Bawah)</option></optgroup><optgroup label="Selangor"><option value='SGR01' class='hs'>SGR01 - Gombak, Petaling, Sepang, Hulu Langat, Hulu Selangor, S.Alam</option><option value='SGR02' class='hs'>SGR02 - Kuala Selangor, Sabak Bernam</option><option value='SGR03' class='hs'>SGR03 - Klang, Kuala Langat</option></optgroup><optgroup label="Sarawak"><option value='SWK01' class='hs'>SWK01 - Limbang, Lawas, Sundar, Trusan</option><option value='SWK02' class='hs'>SWK02 - Miri, Niah, Bekenu, Sibuti, Marudi</option><option value='SWK03' class='hs'>SWK03 - Pandan, Belaga, Suai, Tatau, Sebauh, Bintulu</option><option value='SWK04' class='hs'>SWK04 - Sibu, Mukah, Dalat, Song, Igan, Oya, Balingian, Kanowit, Kapit</option><option value='SWK05' class='hs'>SWK05 - Sarikei, Matu, Julau, Rajang, Daro, Bintangor, Belawai</option><option value='SWK06' class='hs'>SWK06 - Lubok Antu, Sri Aman, Roban, Debak, Kabong, Lingga, Engkelili, Betong, Spaoh, Pusa, Saratok</option><option value='SWK07' class='hs'>SWK07 - Serian, Simunjan, Samarahan, Sebuyau, Meludam</option><option value='SWK08' class='hs'>SWK08 - Kuching, Bau, Lundu, Sematan</option><option value='SWK09' class='hs'>SWK09 - Zon Khas (Kampung Patarikan)</option></optgroup><optgroup label="Terengganu"><option value='TRG01' class='hs'>TRG01 - Kuala Terengganu, Marang, Kuala Nerus</option><option value='TRG02' class='hs'>TRG02 - Besut, Setiu</option><option value='TRG03' class='hs'>TRG03 - Hulu Terengganu</option><option value='TRG04' class='hs'>TRG04 - Dungun, Kemaman</option></optgroup><optgroup label="Wilayah Persekutuan"><option value='WLY01' class='hs'>WLY01 - Kuala Lumpur, Putrajaya</option><option value='WLY02' class='hs'>WLY02 - Labuan</option></optgroup>
                          </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please fill the name
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn btn-light-dark" data-bs-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                                <button class="btn btn-primary submit-fn mt-2" name="BtnSaveBox" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <?php require "footer.php" ?>


            <script type="text/javascript">
                $('#subscriber').click(function() {
                    $('#exampleModal').modal('show');
                });
                $('.form-custom-switch-dual-label').change(function(){
                    var box_id = $(this).val();
                    if($(this).prop('checked')){
                        var status=1;
                    } else {
                        var status=0;
                    }
                    $.ajax({
                        type: 'POST',
                        url: 'ajax/update_status_box.php',
                        data: {
                            'box_id':  box_id,'status':  status
                        },
                        dataType : 'json',
                        success: function (data) {
                            if (data.success) {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000,
                                    timerProgressBar: true,
                                    didOpen: (toast) => {
                                        toast.addEventListener('mouseenter', Swal.stopTimer)
                                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })
                                    
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Status changed'
                                })
                            } else {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000,
                                    timerProgressBar: true,
                                    didOpen: (toast) => {
                                        toast.addEventListener('mouseenter', Swal.stopTimer)
                                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                                    }
                                })
                                    
                                Toast.fire({
                                    icon: 'error',
                                    title: '"Error: " + data.message'
                                })
                            }
                        },
                        error: function(xhr, status, error) {
                            // Handle AJAX call errors
                            console.error("AJAX Error: " + status + " - " + error);
                            alert("An error occurred while processing the request.");
                        }
                    });
                });

            </script>
